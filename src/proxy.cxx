////////////////////////////////////////////////////////////////////////////
//      proxy.cc
//
//      Implementation of the IS proxy library
//
//      Sergei Kolos August 2007
//
//      description:
//              This file contains implementation of the IS service proxy
//
////////////////////////////////////////////////////////////////////////////
#include <unistd.h>
#include <memory>
#include <mutex>

#include <ipc/partition.h>
#include <ipc/applicationcontext.h>
#include <ipc/partitioncontext.h>
#include <ipc/proxy/factory.h>

#include <is/is.hh>
#include <is/infoany.h>
#include <is/infoprovider.h>

#include <oh/OHRawProvider.h>

#include <MonInfoGatherer/Config.h>
#include <MonInfoGatherer/DefaultHandler.h>
#include <MonInfoGatherer/InfoPublisher.h>
#include <oh/core/Histogram.h>

template <class T>
class ISProxy : public POA_is::repository_tie<T>, public MIG::InfoPublisher
{
  public:
    ISProxy(T * remote)
      : POA_is::repository_tie<T>(remote),
      	m_histogram_type(oh::Histogram::type()),
        m_gatherer(MIG::HandlerConfig(".*", "", {}), *this),
        m_provider(0)
    {
	IPCPartitionContext remote_context(remote);
        m_partition = IPCPartition(remote_context.m_partition);
        m_server_name = remote_context.m_name;
        
	ERS_DEBUG(1, "IS proxy gathering plugin has been constructed");
    }

    void create_stream(const is::criteria & criteria, is::stream_ptr stream,
            CORBA::Long history_depth, is::sorted order)
    {
        this->_tied_object()->create_stream(is::criteria(), stream, history_depth, order);
    }
        
    void create(const is::info & inf)
    {
        if (!handleHistogram(inf)) {
            this->_tied_object()->create(inf);
        }
    }
    
    void checkin(const is::info & inf, ::CORBA::Boolean keep_history, ::CORBA::Boolean use_tag)
    {
        if (!handleHistogram(inf)) {
            this->_tied_object()->checkin(inf, keep_history, use_tag);
        }
    }
    
    void update(const is::info & inf, ::CORBA::Boolean keep_history, ::CORBA::Boolean use_tag)
    {
        if (!handleHistogram(inf)) {
            this->_tied_object()->update(inf, keep_history, use_tag);
        }
    }

  private:
    bool handleHistogram(const is::info & inf);
    
    // implementation of InfoPublisher interface
    void publish(ISInfo & info, const std::string & name, int number) override;
    
    void createProvider();

  private:
    typedef MIG::DefaultHandler	Gatherer;
    
    const ISType	m_histogram_type;
    IPCPartition	m_partition;
    std::string		m_server_name;
    std::string		m_provider_name;
    Gatherer		m_gatherer;

    std::mutex		m_provider_mutex;
    OHRawProvider<> *	m_provider;
    bool                m_provider_error = false;
};

template <class T>
bool
ISProxy<T>::handleHistogram(const is::info & inf)
{
    ERS_DEBUG(2, "information '" << inf.name << "' received");
    ISType t(inf.type);
    if ( !t.subTypeOf( m_histogram_type ) ) {
    	return false;
    }
    
    ERS_DEBUG(2, "pass '" << inf.name << "' histogram to the gatherer");
    ISInfoAny any;
    is::idatastream in(const_cast<is::info&>(inf).value.data);
    any.fromWire(m_partition, inf.name, inf.type, inf.value, in);
    
    std::string::size_type pos = 0;
    while ( inf.name[pos] != '.' && inf.name[pos] != 0 ) {
    	++pos;
    }
    
    if (inf.name[pos] == 0) {
    	return false;
    }
        
    std::string p(inf.name, pos);
    std::string o(inf.name + pos + 1);
    
    try {
	m_gatherer.updateInfo(any, o, p);

	ERS_DEBUG(3, "histogram '" << inf.name << "' is successfully processed");
	return true;
    }
    catch(ers::Issue & ex){
	ers::log(ex);
    }
    catch(std::exception & ex) {
	ERS_LOG("std::exception '" << ex.what() << "' was received");
    }
    catch(...){
	ERS_LOG("unknown exception was received");
    }

    return false;
}

template <class T>
void 
ISProxy<T>::publish(ISInfo & isi, const std::string & name, int )
{
    if (not m_provider and not m_provider_error) {
        createProvider();
    }

    std::string oid(m_provider_name + "." + name);
    ERS_DEBUG(2, "histogram '" << oid << "' will be published");

    is::info info;
    is::odatastream out;
    isi.toWire(oid, info, out);
    try {
        this->_tied_object()->checkin(info, true, true);
    }
    catch (CORBA::UserException & ex) {
        ers::log(daq::ipc::CorbaUserException( ERS_HERE, ex._name() ));
    }
    catch (CORBA::SystemException & ex) {
        ers::log(daq::ipc::CorbaSystemException( ERS_HERE, &ex ));
    }
    catch(std::exception & ex) {
        ERS_LOG("std::exception '" << ex.what() << "' was received");
    }
    catch(...){
        ERS_LOG("Unknown exception was received while trying to publish histogram");
    }
}

template <class T>
void
ISProxy<T>::createProvider()
{
    std::unique_lock<std::mutex> lock(m_provider_mutex);

    if (!m_provider) {
        const char * appname = ::getenv("TDAQ_APPLICATION_NAME");

        if (appname == 0) {
            char buffer[256];
            ::gethostname(buffer, sizeof(buffer));
            std::string host(buffer);
            m_provider_name = host.substr(0, host.find('.'));
        } else {
            m_provider_name = appname;
        }

        try {
            m_provider = new OHRawProvider<>(m_partition, m_server_name, m_provider_name);
            ERS_LOG("'" << m_provider_name << "' histogram provider has been created");
        } catch (ers::Issue &) {
            m_provider_error = true;
        }
    }
}

namespace
{
    IPCProxyFactory<is::repository, POA_is::repository, ISProxy> __f1__;
}
